# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'currency_change/version'

Gem::Specification.new do |spec|
  spec.name          = "currency_change"
  spec.version       = CurrencyChange::VERSION
  spec.authors       = ["Thomas HV Corley"]
  spec.email         = ["thomcorley@gmail.com"]
  spec.description   = %q{Converts currencies}
  spec.summary       = %q{Uses data from the ECB. Defaults to the rate for Date.today}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = [
      # `git ls-files`.split($/),
      'lib/currency_change.rb'
  ]
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'engtagger', '~> 0'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake', '~> 0'
  spec.add_development_dependency 'rspec', '~> 0'
end